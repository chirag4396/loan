@extends('user.layouts.master')

@section('content')
<div class="wrapper">
	<section id="banner" style="padding: 100px 0px 0px 0px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="block">
						<form id="msform">
							<!-- progressbar -->
							<ul id="progressbar">
								<li class="active">Primary Information</li>
								<li>Occupation</li>
								<li>Personal Details</li>
							</ul>
							<!-- fieldsets -->
							<fieldset>
								<h2 class="fs-title">Fill your Detail</h2>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected"> How much do you need?</option>
									<option value="Rs 10 lac "> Rs 10 lac </option>
									<option value="Rs 20 lac"> Rs 20 lac</option>
									<option value="Rs 30 lac"> Rs 30 lac</option>   
									<option value="Rs 40 lac"> Rs 40 lac</option>   
									<option value="Rs 50 lac"> Rs 50 lac</option>   
									<option value="Rs 60 lac"> Rs 60 lac</option>   
								</select>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">How much do you earn per year?</option>
									<option value="Rs 2 lac "> Rs 2 lac </option>
									<option value="Rs 3 lac"> Rs 3 lac</option>
									<option value="Rs 4 lac"> Rs 4 lac</option>   
									<option value="Rs 5 lac"> Rs 5 lac</option>   
									<option value="Rs 6 lac"> Rs 6 lac</option>   
								</select>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">Nature of work</option>
									<option value="Rs 1 lac "> Self Employed </option>
									<option value="Rs 2 lac"> Salaried</option>
									<option value="Rs 3 lac"> Professional</option>                 
								</select>
								<input type="text" name="cname" placeholder="Company Name" />

								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">Select your City</option>
									<option value="Rs 1 lac "> Pune </option>
									<option value="Rs 2 lac"> Mumbai</option>
									<option value="Rs 2 lac"> Hyderabad</option>
									<option value="Rs 2 lac"> Bengluru</option>
									<option value="Rs 3 lac"> Delhi</option>                 
								</select>
								<input type="button" name="next" class="next action-button" value="Next" />
							</fieldset>

							<fieldset>
								<h2 class="fs-title">Your Occupation</h2>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">PROPERTY TYPE</option>
									<option value="Rs 1 lac "> Flat </option>
									<option value="Rs 2 lac"> Bunglow</option>
									<option value="Rs 3 lac"> Row House</option>
									<option value="Rs 3 lac"> Office/Shop</option>
								</select>
								<input type="email" name="lname" placeholder="PROPERTY LOCATION" />
								<input type="text" name="phone" placeholder="PROPERTY VALUE" />

								<input type="button" name="previous" class="previous action-button" value="Previous" />
								<input type="button" name="next" class="next action-button" value="Next" />
							</fieldset>

							<fieldset>
								<h2 class="fs-title">Personal Details</h2>
								<input type="text" name="phone" placeholder="Name" />
								<input type="text" name="phone" placeholder="Email " />
								<input type="text" name="phone" placeholder="Mobile Number" />

								<div class="checkout-form-list create-acc">	
									<input id="cbox" type="checkbox" class="mychk"/>
									<label>I accept the terms of service of the website & allow it's representatives to call me with further details. *</label>
								</div>

								<input type="button" name="previous" class="previous action-button" value="Previous" />
								<input type="submit" name="submit" class="submit action-button" value="Submit" />
							</fieldset>


						</form>
					</div>
				</div>
			</div>
		</div>

	</section>

	<section id="blog-left">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-0 col-sm-10 col-sm-offset-1">
					<div class="block myabt">
						<span class="first-child-span">Loan Against Property/ Mortgage Loan Approval</span>
						<p class="first-child">Get instant soft approval from the loan against the property by mortgaging your residential or marketable commercial property. Get up to 100% loan amount on your property’s value within seven working days. Interest rate starts from 8.50% from over 40 Banks & NBFCs.</p>
						<span class="first-child-span">We provide Loan against property on residential, commercial & Industrial Property.</span>
						<p class="first-child">The loan which is provided against any collateral mortgage property is called Property Mortgage Loan or Loan Against Property. Normally all the private sector and public sector banks are aggressively providing property loans in India. Apart from banks, lots of NBFC are also providing mortgage loans in Pune. Due to high competition in these products, there are lots of advantages to loan customers. They get low-interest rates with higher tenure.</p>
						<p class="first-child">In case of loan against property, the approval takes time of 6 to 10 days. Banks take the charge of property at the time of loan disbursal. Key players like HDFC bank, ICICI Bank, ING Vysya Bank, Kotak Mahindra, Axis Bank etc. are the major stake holder from private sector banks in India. Banks interest rates are normally 8.50 % to 16.00 % in reducing the balance.</p>
					</div>						
					<div class="block myabt">							
						<span>Property Loans/ Mortgage Loan</span>
						<ul>
							<li>Property Loans start from Rs 2 Lacs onward depending on your property valuation and your individual eligibility.</li>
							<li>Loan against property can be borrowed up to 100% of market value of the same property.</li>
							<li>Easiest and Flexible for taking option between an EMI based loan or overdraft facilities.</li>
							<li>Property loan is available with the option of high tenure loans for the low burden of repayment.</li>
							<li>Low MCLR rate and maximum tenure with low-interest rates as the comparison with a Personal loan or Business loan.</li>
							<li>Simple documentation and speedy processing.</li>
							<li>Property Loan avail for salaried & self-employed individuals or company also.</li>
						</ul>
					</div>
					<div class="block myabt">							
						<span>Features of Loan Against Property</span>
						<ul>
							<li>It is the second cheapest loan in the market after Home Loan. It works out to be much cheaper than the Business loan which is usually issued at interest rates in the range of 8.50% – 16.00%.</li>
							<li>On the basis of security, they provide longer tenure. The tenure for a Loan Against Property is usually longer than a personal loan. Generally, based on a secured loan tenure goes up to 15 years.</li>
							<li>Due to a secured nature of loan the another quality of LAP is the lower rate of interest which makes this product lowest EMI product.</li>
							<li>LAP requires normal documentation like the personal loan or business loan. Only property documents added for the collateral purpose.</li>
						</ul>
					</div>
				</div>
				<div class="col-md-4  col-xs-12 right-column">

					<div class="widget">
						<span>Process</span>
						<div class="widget-body">
							<ul class="category-list">
								<li>The loan against property goes through the following stages</li>
								<li>Application for Loan</li>
								<li>Processing of Application</li>
								<li>Loan Documentation.</li>
								<li>Initiation for Verification / Valuation of property</li>
								<li>Credit Sanctioning of the Loan</li>
								<li>Disbursement of Loan</li>
							</ul>
						</div>
					</div>
					
					<div class="widget">
						<span>Documentation Required for Loan Against Property</span>
						<div class="widget-body">
							<ul class="category-list">
								<li>Identity Proof</li>
								<li>Residence Address Proof</li>
								<li>6 month Bank Statement</li>
								<li>3 month salary slips</li>
								<li>Previous or current job experience proof</li>
								<li>Form 16 for previous employment / ITR for self employed</li>
								<li>Property Document</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>
@endsection