@extends('user.layouts.master')

@section('content')
<div class="wrapper">
	<section id="banner" style="padding: 100px 0px 0px 0px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="block">
						<form id="msform">
							<!-- progressbar -->
							<ul id="progressbar">
								<li class="active">Primary Information</li>
								<li>Occupation</li>
								<li>Personal Details</li>
							</ul>
							<!-- fieldsets -->
							<fieldset>
								<h2 class="fs-title">Fill your Detail</h2>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected"> How much do you need?</option>
									<option value="Rs 10 lac "> Rs 10 lac </option>
									<option value="Rs 20 lac"> Rs 20 lac</option>
									<option value="Rs 30 lac"> Rs 30 lac</option>   
									<option value="Rs 40 lac"> Rs 40 lac</option>   
									<option value="Rs 50 lac"> Rs 50 lac</option>   
									<option value="Rs 60 lac"> Rs 60 lac</option>   
								</select>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">Your company's last year's turnover?</option>
									<option value="Rs 2 lac "> Rs 2 lac </option>
									<option value="Rs 3 lac"> Rs 3 lac</option>
									<option value="Rs 4 lac"> Rs 4 lac</option>   
									<option value="Rs 5 lac"> Rs 5 lac</option>   
									<option value="Rs 6 lac"> Rs 6 lac</option>   
								</select>

								<input type="button" name="next" class="next action-button" value="Next" />
							</fieldset>

							<fieldset>
								<h2 class="fs-title">Your Occupation</h2>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">Nature of Business</option>
									<option value="Rs 1 lac "> Manufacturing </option>
									<option value="Rs 2 lac"> Service Industry</option>
									<option value="Rs 3 lac"> Trader/Retailer/Distributor</option>
									<option value="Rs 3 lac"> Consulting</option>
									<option value="Rs 3 lac"> Medical</option>
									<option value="Rs 3 lac"> Other</option>                      
								</select>
								<input type="text" name="cname" placeholder="Company Name" />

								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected"> Where is your business located?</option>
									<option value="Rs 1 lac "> Pune </option>
									<option value="Rs 2 lac"> Mumbai</option>
									<option value="Rs 3 lac"> Delhi</option>
								</select>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">  Where do you live?</option>
									<option value="Rs 1 lac "> Pune </option>
									<option value="Rs 2 lac"> Mumbai</option>
									<option value="Rs 2 lac"> Hyderabad</option>
									<option value="Rs 2 lac"> Begluru</option>
									<option value="Rs 3 lac"> Delhi</option>
									<option value="Rs 2 lac"> Other</option>
								</select>

								<input type="button" name="previous" class="previous action-button" value="Previous" />
								<input type="button" name="next" class="next action-button" value="Next" />
							</fieldset>

							<fieldset>
								<h2 class="fs-title">Personal Details</h2>
								<input type="text" name="phone" placeholder="Name" />
								<input type="text" name="phone" placeholder="Email " />
								<input type="text" name="phone" placeholder="Mobile Number" />

								<div class="checkout-form-list create-acc">	
									<input id="cbox" type="checkbox" class="mychk"/>
									<label>I accept the terms of service of the website & allow it's representatives to call me with further details. *</label>
								</div>

								<input type="button" name="previous" class="previous action-button" value="Previous" />
								<input type="submit" name="submit" class="submit action-button" value="Submit" />
							</fieldset>


						</form>
					</div>
				</div>
			</div>
		</div>
		
	</section>


	<section id="blog-left">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-0 col-sm-10 col-sm-offset-1">
					<div class="block myabt">
						<span class="first-child-span"></span>
						<p class="first-child">Avail Business Loan up to Rs. 2 Crore from 40+ Banks and NBFCs. ✓ loan application and approval ✓Minimum documentation ✓No collateral ✓No guarantor required ✓Compare Interest Rates for a business loan ✓Affordable interest rates ✓Calculate your monthly Business Loan EMIs. ✓Flexible loan tenure upto 180 months</p>
						<span class="first-child-span">What is a Business Loan?</span>
						<p class="first-child">Each business is unique & have specific requirements. Business loan is offered by various  banks & financial institutions across the country are customized & customer centric. These are made to offer the business owners with the wide array of financing alternatives.</p>
						<p class="first-child">The small business loan is safest & easiest option to appropriately finance the business objectives. Banks & financial institutions offer tailor made loans based on nature, scope & goal of the requirements.</p>


						<span>DealsOfLoan brings special Doctors Program for MBBS, BDS & Above qualification Interest rate start from @10.99%, Loan up to 5 Years.</span>

						<ul>
							<li>Features</li>
							<p class="first-child">Business loans in can be availed in range of Rs.1,00,000 to Rs. 2 Crore. These loan applications normally get approved quickly without any hassle during the process. Most banks & financial institutions provide the customer with promise of unsecured lending solutions when it comes to the business loan. The business loan come with the added benefit of services, like SMS, Web Chat, Phone Banking, etc. Some banks extend exclusive higher loan amounts to the self employed customers. With minimal paperwork & flexible repayment options, more & more business owners are leaning on business loan to fund the business need.</p>
							<li>Benefits</li>
							<p class="first-child">Unsecured business loan enable businesses to fund exact exigencies & repay the loan amount in easy EMI. These loans are availed for the different reasons, including the business expansions, working capital, etc.</p>
							<p class="first-child">Banks & financial institutions offers business loans to customers with flexibility of the doorstep service. Furthermore, business loans in India normally do not expect any collateral, guarantor or security from an applicant.</p>
							<p class="first-child">Some banks offer customers with a facility of business loan within a minute, either online or through their branches. These speedy approvals assure that customers do not waste their time waiting to hear from banks for status of their loan application.</p>
							<p class="first-child">Business loans are destined to help small businesses to meet their capital & other requirements. Generally, the following categories of people can avail the business loan.</p>
						</ul><br>
					</div>						
					
				</div>
				<div class="col-md-4  col-xs-12 right-column">
					
					<div class="widget">
						<span>Business Loan Eligibility</span>
						<div class="widget-body">
							<ul class="category-list">
								<li><a href="#">Self Employed Professionals</a></li>
								<li>This type of category include, allopathic doctors, chartered accountants, company secretaries, & architects etc. This is subject to the applicants who have the proof of qualifications & are practicing their profession.</li>
								<li><a href="#">Self Employed Non Professionals:</a></li>
								<li>This type of category includes traders & manufacturers.</li>
							</ul>
						</div>
					</div>
					
					<div class="widget">
						<span>Who can avail Business Loan?</span>
						<div class="widget-body">
							<ul class="category-list">
								<li>Following entities can avail the business loan:</li>
								<li>Proprietorship</li>
								<li>Partnerships</li>
								<li>Closely Held Limited Companies</li>
								<li>Private Limited Companies</li>
								<li>Limited Liability Partnerships</li>
								<li>Applicants have to fulfil following criteria in order to be eligible for business loan:</li>
								<li>No Minimum Turnover</li>
								<li>Min 3 years experience in the current business</li>
								<li>Min 3 years of business experience</li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>

</div>
@endsection