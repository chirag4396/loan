@extends('user.layouts.master')

@section('content')
<div class="wrapper">
	<section id="banner">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="block">
						<h1>Apply for Loans Credit Cards & Insurance</h1>
						<h2>We started with the aim of making the complete loan process as easy as possible so that every individual can have access to credit easily. We believe the way we have used our technology will help everyone to meet their financial requirement in just one click.</h2>
						<div class="buttons">								
							<a href="personalloan.html" target="_blank" class="btn btn-learn">Personal Loan</a>

							<a href="homeloan.html" target="_blank" class="btn btn-learn">Home Loan</a>

							<a href="loanagainstproperty.html" target="_blank" class="btn btn-learn">Loan Against Property</a>

							<a href="businessloan.html" target="_blank" class="btn btn-learn">Business Loan</a>
							
							<a href="creditcard.html" target="_blank" class="btn btn-learn">Credit Card</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="scrolldown">
			<a id="scroll" href="#features" class="scroll"></a>
		</div>
	</section>
	<section id="features">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2>OUR BEST SERVICES</h2>
						<p>Dantes remained confused and silent by this explanation of the <br> thoughts which had unconsciously</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-xs-12 col-sm-6">
					<div class="feature-block text-center">
						<div class="icon-box">
							<i class="ion-easel"></i>
						</div>
						<h4 class="wow fadeInUp" data-wow-delay=".3s">Responsive Design</h4>
						<p class="wow fadeInUp" data-wow-delay=".5s">Lorem ipsum dolor sit amet, consectetur adipisic-<br>ing elit, sed do eiusmod tempor incididunt ut <br> labore et dolore magna aliqua. Ut enim ad minim</p>
					</div>
				</div>
				<div class="col-md-4 col-xs-12 col-sm-6">
					<div class="feature-block text-center">
						<div class="icon-box">
							<i class="ion-paintbucket"></i>
						</div>
						<h4 class="wow fadeInUp" data-wow-delay=".3s">Outstanding Animation</h4>
						<p class="wow fadeInUp" data-wow-delay=".5s">Lorem ipsum dolor sit amet, consectetur adipisic-<br>ing elit, sed do eiusmod tempor incididunt ut <br> labore et dolore magna aliqua. Ut enim ad minim</p>
					</div>
				</div>
				<div class="col-md-4 col-xs-12 col-sm-6">
					<div class="feature-block text-center">
						<div class="icon-box">
							<i class="ion-paintbrush"></i>
						</div>
						<h4 class="wow fadeInUp" data-wow-delay=".3s">Unlimited Colors</h4>
						<p class="wow fadeInUp" data-wow-delay=".5s">Lorem ipsum dolor sit amet, consectetur adipisic-<br>ing elit, sed do eiusmod tempor incididunt ut <br> labore et dolore magna aliqua. Ut enim ad minim</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="counter">
		<div class="container">
			<div class="row">
				<div class="title">
					<h2>FUN FACTS</h2>
					<p>Dantes remained confused and silent by this explanation of the <br> thoughts which had unconsciously</p>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="block wow fadeInRight" data-wow-delay=".3s">
						<i class="ion-code"></i>
						<p class="count-text">
							<span class="counter-digit">136800 </span> k
						</p>
						<p>Lines Coded</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="block wow fadeInRight" data-wow-delay=".5s">
						<i class="ion-compass"></i>
						<p class="count-text">
							<span class="counter-digit">7800 </span> +
						</p>
						<p>Lines Coded</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="block wow fadeInRight" data-wow-delay=".7s">
						<i class="ion-compose"></i>
						<p class="count-text">
							<span class="counter-digit">399</span>
						</p>
						<p>Lines Coded</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="block wow fadeInRight" data-wow-delay=".9s">
						<i class="ion-image"></i>
						<p class="count-text">
							<span class="counter-digit">9995</span>
						</p>
						<p>Lines Coded</p>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section id="blog">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2>OUR PROMISE</h2>
						<p>Dantes remained confused and silent by this explanation of the <br> thoughts which had unconsciously</p>
					</div>
					<div id="blog-post" class="owl-carousel">
						<div>
							<div class="block">
								<img src="img/blog/blog-1.jpg" alt="" class="img-responsive">
								<div class="content">
									<h4><a href="blog.html">Hey,This is a blog title</a></h4>
									<small>By admin / Sept 18, 2014</small>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ex itaque repudiandae nihil qui debitis atque necessitatibus aliquam, consequuntur autem!
									</p>
									<a href="blog.html" class="btn btn-read">Read More</a>
									
								</div>
							</div>
						</div>
						<div>
							<div class="block">
								<img src="img/blog/blog-2.jpg" alt="" class="img-responsive">
								<div class="content">
									<h4><a href="blog.html">Hey,This is a blog title</a></h4>
									<small>By admin / Sept 18, 2014</small>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ex itaque repudiandae nihil qui debitis atque necessitatibus aliquam, consequuntur autem!
									</p>
									<a href="blog.html" class="btn btn-read">Read More</a>
									
								</div>
							</div>
						</div>
						<div>
							<div class="block">
								<img src="img/blog/blog-3.jpg" alt="" class="img-responsive">
								<div class="content">
									<h4><a href="blog.html">Hey,This is a blog title</a></h4>
									<small>By admin / Sept 18, 2014</small>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ex itaque repudiandae nihil qui debitis atque necessitatibus aliquam, consequuntur autem!
									</p>
									<a href="blog.html" class="btn btn-read">Read More</a>
									
								</div>
							</div>
						</div>
						<div>
							<div class="block">
								<img src="img/blog/blog-4.jpg" alt="" class="img-responsive">
								<div class="content">
									<h4><a href="blog.html">Hey,This is a blog title</a></h4>
									<small>By admin / Sept 18, 2014</small>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ex itaque repudiandae nihil qui debitis atque necessitatibus aliquam, consequuntur autem!
									</p>
									<a href="blog.html" class="btn btn-read">Read More</a>
									
								</div>
							</div>
						</div>
						<div>
							<div class="block">
								<img src="img/blog/blog-1.jpg" alt="" class="img-responsive">
								<div class="content">
									<h4><a href="blog.html">Hey,This is a blog title</a></h4>
									<small>By admin / Sept 18, 2014</small>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ex itaque repudiandae nihil qui debitis atque necessitatibus aliquam, consequuntur autem!
									</p>
									<a href="blog.html" class="btn btn-read">Read More</a>
									
								</div>
							</div>
						</div>
						
					</div>		
				</div>
			</div>
		</div>
	</section>



	
	<section id="play-video">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="block">
						<h2 class="wow fadeInUp" data-wow-delay=".3s">GET INSTANT LOAN</h2>
						<p class="wow fadeInUp" data-wow-delay=".5s">Dantes remained confused and silent by this explanation of the </p>
						<a href="#" class="html5lightbox" data-width=800 data-height=400>
							<div class="button ion-ios-play-outline wow zoomIn" data-wow-delay=".3s"></div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	



	<section id="testimonial">
		<div class="container">
			<div class="row">
				<div class="title">
					<h2>TESTIMONIAL</h2>
					<p>Dantes remained confused and silent by this explanation of the <br> thoughts which had unconsciously</p>
				</div>
				<div class="col col-md-6">
					<div class="media wow fadeInLeft" data-wow-delay=".3s">
						<div class="media-left">
							<a href="#">
								<img src="img/service-img.png" alt="">
							</a>
						</div>
						<div class="media-body">
							<a href="#"><h4 class="media-heading">Jonathon Andrew</h4></a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo</p>
						</div>
					</div>
				</div>
				<div class="col col-md-6">
					<div class="media wow fadeInRight" data-wow-delay=".3s">
						<div class="media-left">
							<a href="#">
								<img src="img/service-img.png" alt="">
							</a>
						</div>
						<div class="media-body">
							<a href="#"><h4 class="media-heading">Jonathon Andrew</h4></a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo</p>
						</div>
					</div>
				</div>
				<div class="col col-md-6">
					<div class="media wow fadeInLeft" data-wow-delay=".3s">
						<div class="media-left">
							<a href="#">
								<img src="img/service-img.png" alt="">
							</a>
						</div>
						<div class="media-body">
							<a href="#"><h4 class="media-heading">Jonathon Andrew</h4></a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo</p>
						</div>
					</div>
				</div>
				<div class="col col-md-6">
					<div class="media wow fadeInRight" data-wow-delay=".3s">
						<div class="media-left">
							<a href="#">
								<img src="img/service-img.png" alt="">
							</a>
						</div>
						<div class="media-body">
							<a href="#"><h4 class="media-heading">Jonathon Andrew</h4></a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="client-logo">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="block">
						<div id="Client_Logo" class="owl-carousel">
							<div>
								<a href="#"><img class="img-responsive" src="img/clientLogo/client-logo1.jpg" alt=""></a>
							</div>
							<div>
								<a href="#"><img class="img-responsive" src="img/clientLogo/client-logo2.jpg" alt=""></a>
							</div>
							<div>
								<a href="#"><img class="img-responsive" src="img/clientLogo/client-logo3.jpg" alt=""></a>
							</div>
							<div>
								<a href="#"><img class="img-responsive" src="img/clientLogo/client-logo4.jpg" alt=""></a>
							</div>
							<div>
								<a href="#"><img class="img-responsive" src="img/clientLogo/client-logo5.jpg" alt=""></a>
							</div>
							<div>
								<a href="#"><img class="img-responsive" src="img/clientLogo/client-logo6.jpg" alt=""></a>
							</div>
							<div>
								<a href="#"><img class="img-responsive" src="img/clientLogo/client-logo1.jpg" alt=""></a>
							</div>
							<div>
								<a href="#"><img class="img-responsive" src="img/clientLogo/client-logo2.jpg" alt=""></a>
							</div>
							<div>
								<a href="#"><img class="img-responsive" src="img/clientLogo/client-logo3.jpg" alt=""></a>
							</div>
							<div>
								<a href="#"><img class="img-responsive" src="img/clientLogo/client-logo4.jpg" alt=""></a>
							</div>
							<div>
								<a href="#"><img class="img-responsive" src="img/clientLogo/client-logo5.jpg" alt=""></a>
							</div>
							<div>
								<a href="#"><img class="img-responsive" src="img/clientLogo/client-logo6.jpg" alt=""></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>	
@endsection