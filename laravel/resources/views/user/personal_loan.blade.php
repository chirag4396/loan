@extends('user.layout.master')

@section('content')
<div class="wrapper">
	<section id="banner" style="padding: 100px 0px 0px 0px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="block">
						<form id="msform">
							<!-- progressbar -->
							<ul id="progressbar">
								<li class="active">Primary Information</li>
								<li>Occupation</li>
								<li>Personal Details</li>
							</ul>
							<!-- fieldsets -->
							<fieldset>
								<h2 class="fs-title">Fill your Detail</h2>
								<input type="text" name="email" placeholder="How much do you need ?" />
								<input type="text" name="pass" placeholder="How much do you earn per year?" />

								<input type="button" name="next" class="next action-button" value="Next" />
							</fieldset>

							<fieldset>
								<h2 class="fs-title">Your Occupation</h2>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">Nature of work</option>
									<option value="Rs 1 lac "> Self Employed </option>
									<option value="Rs 2 lac"> Salaried</option>
									<option value="Rs 3 lac"> Professional</option>                      
								</select>
								<input type="text" name="cname" placeholder="Company Name" />

								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">Select your City</option>
									<option value="Rs 1 lac "> Pune </option>
									<option value="Rs 2 lac"> Mumbai</option>
									<option value="Rs 3 lac"> Delhi</option>                      
								</select>

								<input type="button" name="previous" class="previous action-button" value="Previous" />
								<input type="button" name="next" class="next action-button" value="Next" />
							</fieldset>

							<fieldset>
								<h2 class="fs-title">Personal Details</h2>
								<input type="text" name="name" placeholder="Your Name" />
								<input type="email" name="lname" placeholder="Email" />
								<input type="text" name="phone" placeholder="Phone" />

								<div class="checkout-form-list create-acc">	
									<input id="cbox" type="checkbox" class="mychk"/>
									<label>I accept the terms of service of the website & allow it's representatives to call me with further details. *</label>
								</div>

								<input type="button" name="previous" class="previous action-button" value="Previous" />
								<input type="submit" name="submit" class="submit action-button" value="Submit" />
							</fieldset>


						</form>
					</div>
				</div>
			</div>
		</div>

	</section>
	<section id="blog-left">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-0 col-sm-10 col-sm-offset-1">
					<div class="block myabt">
						<span class="first-child-span">Personal Loan Real time Approval</span>
						<p class="first-child">Dealsofloan offers instant soft approval for Personal Loan which further gets disbursed in 48 hours from our in-house NBFC, multiple banks & other NBFCs. Enjoy the fastest process, door step services, transparent processing, simple & easy documentation and competitive lowest interest rate.</p>
						
						<span>Personal Loan offers / schemes, lowest interest rates from different financial companies.</span>

						<ul>
							<li>We are here to help all our customers to get special offers from all the Banks/ NBFCs for quote on interest rates in many different ways (ex. monthly reducing rate, flat rate). Looking at the interest rate value alone might not give you a true picture of which personal loan quote is the best for you. You must check the EMI. The processing fee should also be considered when comparing loan quotes.</li>
							<li>If you are in urgent need of Personal loan, just compare the average time taken by the banks to disburse a personal loan. Get Instant Personal Loan in Pune.</li>
							<li>Compare the customer satisfaction ratings of each bank and choose the best, as the application process is different at each bank.</li>
							<li>You can take help for comparing the best deal from DealsofLoan team at free of cost.</li>
						</ul>


						<p>A personal loan is purely unsecured, short term cash loan, which means that the individual customer is not required to provide the bank with any security (ex. property, jewels) or any kind of guaranty against the personal loan.Formally, for any urgent cash requirement, we prefer personal loan. It can be taken for any general purposes like education, reconstruction of property/home renovation, a wedding expense, vacation etc. Most of the finance companies or other banks offer Personal Loan up to Rs. 40 lacs for salaried customers. Normally, it can be repaid over a period of 12 months to 60 months. Some of the banks are funding more than that.</p>
						<p>The personal loan amount you are eligible for and the personal loan interest rate for you will depend on factors such as monthly income, customers employment history, residence history and past financial credit history along with the pattern. Banks typically have capped the monthly payment (EMI) on your loan to about approximately 60% – 70% of your monthly take home income. However, a customer cannot take a personal loan for any kind of bad investment or expense which is not approved by the banks or by the law.</p><br>

					</div>						

				</div>
				<div class="col-md-4  col-xs-12 right-column">

					<div class="widget">
						<span>Personal Loan Eligibility</span>
						<div class="widget-body">
							<ul class="category-list">
								<li>Minimum age of Applicant : 21 years</li>
								<li>Maximum age of Applicant at loan maturity :  60 years</li>
								<li>Minimum Net Monthly Income : Rs. 15,000 per month (minimum Rs. 15,000 – Rs. 20,000 in some banks according to their loan eligibility criteria)</li>
							</ul>
						</div>
					</div>



					<div class="widget">
						<span>Documents Required for Personal Loan</span>
						<div class="widget-body">
							<h6><b>For Salaried Customers</b></h6>
							<ul class="category-list">
								<li>One valid Identity proof</li>
								<li>Latest six months Bank statements</li>
							</ul>
						</div>							
					</div>

					<div class="widget">
						<span>Personal Loan Offers / Schemes / Interest Rates</span>
						<div class="widget-body">
							<ul class="category-list">
								<li><a href="#">Agra Personal Loans</a></li>
								<li><a href="#">Ahmedabad Personal Loans</a></li>
								<li><a href="#">Ahmednagar Personal Loans</a></li>
								<li><a href="#">Ajmer Personal Loans</a></li>
							</ul>
						</div>
					</div>						
				</div>
			</div>
		</div>
	</section>
</div>
@endsection