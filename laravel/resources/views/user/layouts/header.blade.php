<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>MyLoan</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Css link -->
	<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
	<link rel="stylesheet" href="{{ asset('css/owl.transitions.css') }}">
	<link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/lightbox.css') }}">
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/preloader.css') }}">
	<link rel="stylesheet" href="{{ asset('css/image.css') }}">
	<link rel="stylesheet" href="{{ asset('css/icon.css') }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

</head>
<body id="top">
	<header id="navigation" class="navbar-fixed-top animated-header">
		<div class="container">
			<div class="navbar-header">
				<!-- responsive nav button -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!-- /responsive nav button -->			
				<!-- logo -->
				<h1 class="navbar-brand">
					<a href="ro"><img src="img/logo.png" width="200" alt=""></a>
				</h1>
				<!-- /logo -->
			</div>
			<!-- main nav -->
			<nav class="collapse navbar-collapse navbar-right" role="navigation">
				<ul id="nav" class="nav navbar-nav menu">
					<li class="active"><a href="{{ route('home') }}">Home</a></li>
					<li><a href="{{ route('personal-loan') }}">Personal Loan</a></li>
					<li><a href="{{ route('home-loan') }}">Home Loan</a></li>  
					<li><a href="{{ route('loan-against-property') }}">Loan Against Property</a></li>
					<li><a href="{{ route('business-loan') }}">Business Loan</a></li>
					<li><a href="{{ route('credit-card') }}">Credit Card</a></li>
				</ul>
			</nav>
			<!-- /main nav -->
			
		</div>
	</header>
