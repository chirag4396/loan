@extends('user.layout.master')

@section('content')
<div class="wrapper">
	<section id="banner" style="padding: 100px 0px 0px 0px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="block">
						<form id="msform">
							<!-- progressbar -->
							<ul id="progressbar">
								<li class="active">Primary Information</li>
								<li>Occupation</li>
								<li>Personal Details</li>
							</ul>
							<!-- fieldsets -->
							<fieldset>
								<h2 class="fs-title">Fill your Detail</h2>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected"> How much do you need?</option>
									<option value="Rs 10 lac "> Rs 10 lac </option>
									<option value="Rs 20 lac"> Rs 20 lac</option>
									<option value="Rs 30 lac"> Rs 30 lac</option>   
									<option value="Rs 40 lac"> Rs 40 lac</option>   
									<option value="Rs 50 lac"> Rs 50 lac</option>   
									<option value="Rs 60 lac"> Rs 60 lac</option>   
								</select>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">How much do you earn per year?</option>
									<option value="Rs 2 lac "> Rs 2 lac </option>
									<option value="Rs 3 lac"> Rs 3 lac</option>
									<option value="Rs 4 lac"> Rs 4 lac</option>   
									<option value="Rs 5 lac"> Rs 5 lac</option>   
									<option value="Rs 6 lac"> Rs 6 lac</option>   
								</select>							    
								<input type="button" name="next" class="next action-button" value="Next" />
							</fieldset>

							<fieldset>
								<h2 class="fs-title">Your Occupation</h2>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">Nature of work</option>
									<option value="Rs 1 lac "> Self Employed </option>
									<option value="Rs 2 lac"> Salaried</option>
									<option value="Rs 3 lac"> Professional</option>                      
								</select>
								<input type="text" name="cname" placeholder="Company Name" />

								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">Select your City</option>
									<option value="Rs 1 lac "> Pune </option>
									<option value="Rs 2 lac"> Mumbai</option>
									<option value="Rs 3 lac"> Delhi</option>
									<option value="Rs 3 lac"> Hyderabad</option>
									<option value="Rs 3 lac"> Bengluru</option>               
								</select>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">PROPERTY TYPE</option>
									<option value="Rs 1 lac "> Flat </option>
									<option value="Rs 2 lac"> Bunglow</option>
									<option value="Rs 3 lac"> Row House</option>
									<option value="Rs 3 lac"> Office/Shop</option>
								</select>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">POSSESSION</option>
									<option value="Rs 1 lac">Ready Possession </option>
									<option value="Rs 2 lac">Under Contruction</option>
									<option value="Rs 3 lac">Resale Property</option>
								</select>
								<input type="text" name="phone" placeholder="Property Location" />
								<input type="text" name="phone" placeholder="Property value" />

								<input type="button" name="previous" class="previous action-button" value="Previous" />
								<input type="button" name="next" class="next action-button" value="Next" />
							</fieldset>

							<fieldset>
								<h2 class="fs-title">Personal Details</h2>
								<input type="text" name="phone" placeholder="Name" />
								<input type="text" name="phone" placeholder="Email " />
								<input type="text" name="phone" placeholder="Mobile Number" />

								<div class="checkout-form-list create-acc">	
									<input id="cbox" type="checkbox" class="mychk"/>
									<label>I accept the terms of service of the website & allow it's representatives to call me with further details. *</label>
								</div>

								<input type="button" name="previous" class="previous action-button" value="Previous" />
								<input type="submit" name="submit" class="submit action-button" value="Submit" />
							</fieldset>


						</form>
					</div>
				</div>
			</div>
		</div>
		
	</section>
	<section id="blog-left">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-0 col-sm-10 col-sm-offset-1">
					<div class="block myabt">
						<span class="first-child-span"></span>
						<p class="first-child"><b>myloan.com</b> provides the lowest rate of the market. You can compare all the loan providers offer under a single roof. A home loan can be used to buy a new property like flats, bungalow, and row houses also. Banks generally provide 80% to 90% of the property value as a home loan. The remain balance has to be arranged as the initial down payment. You can also avail a personal loan for further down payment amount. Nowadays some builders provide fully furnished flats to enable higher loan amount. This also reduces the cost of furnishing the house on possession. Home loans are available from 5 lac to 25 Cr. myloan.com suggests you the lowest rate of interest.</p>
						<p class="first-child">After applying for a Home Loan there are lots of parts included in availing Home Loan starting with identifying your property to filling up the loan application form. Following are the important stages in Home Loan approvals:</p>
						<span class="first-child-span">Tax Benefits in Home Loan</span>
						<p class="first-child">The home loan borrower enjoys Tax Benefits on both Interest paid & the Principal re-paid. Under Section 24(d) of Income Tax, the deduction of interest payable on the home loan is up to a maximum of Rs. 1,50,000.</p>
						<p class="first-child">Under Section 80(c) of Income Tax, Principal amount for the repayment of loan along with other savings & investments is eligible for tax deduction up to a maximum limit of Rs. 1, 00,000.</p>
					</div>						
					
				</div>
				<div class="col-md-4  col-xs-12 right-column">
					
					<div class="widget">
						<span>Documents Required</span>
						<div class="widget-body">
							<ul class="category-list">
								<li>Colour Photo</li>
								<li>PAN Card Copy</li>
								<li>Last 6 months bank A/C statement</li>
							</ul>
						</div>
					</div>
					<div class="widget">
						<span>Quick Links</span>
						<div class="widget-body">
							<ul class="category-list">
								<li><a href="#">Agra Personal Loans</a></li>
								<li><a href="#">Ghaziabad Home Loans</a></li>
								<li><a href="#">Gurgaon Home Loans</a></li>
								<li><a href="#">Jaipur Home Loans</a></li>
							</ul>
						</div>
					</div>						
				</div>
			</div>
		</div>
	</section>	
</div>
@endsection