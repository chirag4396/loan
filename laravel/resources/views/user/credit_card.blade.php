@extends('user.layouts.master')

@section('content')
<div class="wrapper">
	<section id="banner" style="padding: 100px 0px 0px 0px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="block">
						<form id="msform">
							<!-- progressbar -->
							<ul id="progressbar">
								<li class="active">Primary Information</li>
								<li>Occupation</li>
								<li>Personal Details</li>
							</ul>
							<!-- fieldsets -->
							<fieldset>
								<h2 class="fs-title">Fill your Detail</h2>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">How much do you earn per year?</option>
									<option value="Rs 2 lac "> Rs 2 lac </option>
									<option value="Rs 3 lac"> Rs 3 lac</option>
									<option value="Rs 4 lac"> Rs 4 lac</option>   
									<option value="Rs 5 lac"> Rs 5 lac</option>   
									<option value="Rs 6 lac"> Rs 6 lac</option>   
								</select>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">How much do you earn per month?</option>
									<option value="Rs 2 lac "> Rs 2 lac </option>
									<option value="Rs 3 lac"> Rs 3 lac</option>
									<option value="Rs 4 lac"> Rs 4 lac</option>   
									<option value="Rs 5 lac"> Rs 5 lac</option>   
									<option value="Rs 6 lac"> Rs 6 lac</option>   
								</select>							    
								<input type="button" name="next" class="next action-button" value="Next" />
							</fieldset>

							<fieldset>
								<h2 class="fs-title">Your Occupation</h2>
								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">Nature of work</option>
									<option value="Rs 1 lac "> Self Employed </option>
									<option value="Rs 2 lac"> Salaried</option>
									<option value="Rs 3 lac"> Professional</option>                      
								</select>
								<input type="text" name="cname" placeholder="Company Name" />

								<select class="skin" id="bbm" name="ba" >
									<option value="-1" selected="selected">Select your City</option>
									<option value="Rs 1 lac "> Pune </option>
									<option value="Rs 2 lac"> Mumbai</option>
									<option value="Rs 3 lac"> Delhi</option>
									<option value="Rs 3 lac"> Hyderabad</option>
									<option value="Rs 3 lac"> Bengluru</option>               
								</select>								

								<input type="button" name="previous" class="previous action-button" value="Previous" />
								<input type="button" name="next" class="next action-button" value="Next" />
							</fieldset>

							<fieldset>
								<h2 class="fs-title">Personal Details</h2>
								<input type="text" name="phone" placeholder="Name" />
								<input type="text" name="phone" placeholder="Email " />
								<input type="text" name="phone" placeholder="Mobile Number" />
								<input type="text" name="phone" placeholder="Date of Birth " />

								<div class="checkout-form-list create-acc">	
									<input id="cbox" type="checkbox" class="mychk"/>
									<label>I accept the terms of service of the website & allow it's representatives to call me with further details. *</label>
								</div>

								<input type="button" name="previous" class="previous action-button" value="Previous" />
								<input type="submit" name="submit" class="submit action-button" value="Submit" />
							</fieldset>


						</form>
					</div>
				</div>
			</div>
		</div>
		
	</section>
	


	<section id="blog-left">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-0 col-sm-10 col-sm-offset-1">
					<div class="block myabt">
						<span class="first-child-span">Apply for Credit Card</span>
						<p class="first-child">Credit Card also termed as plastic money allows the cardholders to pay later for the purchases made now. In short it offers interest free credit facility to the cardholders for a pre-defined period of time. Credit cards have been quite popular simply because of their convenience, as one need not carry cash while traveling or for shopping as it is cumbersome and risky also. A credit card can be a very useful instrument if used wisely or else it can make cardholders life miserable.</p>

						<p>It is very essential to understand the terms and conditions about the usage of credit card before one starts using it. There is no harm in asking questions to the credit card issuing company if the cardholder has any doubts related to it. Apart from free credit facility, credit cards offer various facilities like revolving credit i.e. pay minimum balance every month, cash withdrawal from ATM, converting purchase into EMI, balance transfer from another card at minimal or zero cost, etc. Though these facilities offered with credit cards are useful, it can be a long-term debt trap too. Hence, one has to be aware about the implications of availing services offered with cards while managing the debt at manageable level.</p>


						<span class="first-child-span">Credit Card Benefits</span>

						<p>In addition to above benefits attached with credit cards, the credit card issuing companies offer other benefits linked with your spends on the card. There are benefits attached to card like reward points which can be redeemed against renewal fees or gift coupons, waiver of surcharges on railway bookings or on fuel, frequent flyers miles – redeemable points for regular flyers, discounts / cash back on specific purchases, etc.</p>
						<p>Depending on the facilities and additional benefits provided on the credit cards, they are generally categorized as Classic Card, Gold Card, Titanium Card, Platinum Card and Privilege Cards and mostly affiliated to international payment gateways like American Express, MasterCard or Visa.</p>

						<p>It is always advisable to pay your credit card dues on time so as to have a good credit history and maintaining good credit score in CIBIL. The lenders access this history, whenever the cardholder applies for any credit card or loans in future. A bad history or credit score can be a cause for rejection of credit card or any loan.</p><br>
						
					</div>						
					
				</div>
				<div class="col-md-4  col-xs-12 right-column">
					
					<div class="widget">
						<span>Credit Card Eligibility</span>
						<div class="widget-body">
							<ul class="category-list">
								<li><a href="#">Self Employed Professionals</a></li>
								<li>This type of category include, allopathic doctors, chartered accountants, company secretaries, & architects etc. This is subject to the applicants who have the proof of qualifications & are practicing their profession.</li>
								<li><a href="#">Self Employed Non Professionals:</a></li>
								<li>This type of category includes traders & manufacturers.</li>
							</ul>
						</div>
					</div>
					
					<div class="widget">
						<span>Who can avail Credit Card?</span>
						<div class="widget-body">
							<ul class="category-list">								   
								<li>Applicants have to fulfil following criteria in order to be eligible for business loan:</li>
								<li>No Minimum Turnover</li>
								<li>Min 3 years experience in the current business</li>
								<li>Min 3 years of business experience</li>
								<li>Business should make profit for last 2 years</li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>

</div>
@endsection