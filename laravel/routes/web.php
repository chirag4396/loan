<?php

Route::get('/', function () {
    return view('user.index');
})->name('home');

Route::get('/personal-loan', function () {
    return view('user.personal_loan');
})->name('personal-loan');

Route::get('/home-loan', function () {
    return view('user.home_loan');
})->name('home-loan');

Route::get('/loan-against-property', function () {
    return view('user.loan_against_property');
})->name('loan-against-property');

Route::get('/business-loan', function () {
    return view('user.business_loan');
})->name('business-loan');

Route::get('/credit-card', function () {
    return view('user.credit_card');
})->name('credit-card');